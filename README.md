# Proyecto WSN 
## Wireless Sensor Network

============================

Software para la gestión de redes inalámbricas. Incorpora la siguiente funcionalidad:
* Gestión de nodos de sensores
* Gestión de paquetes de datos recibidos.
* Integración de dispositivos waspmote y Plug&Sense
* Gestión de imágenes para la generación de imágenes de producción de Índice de Vegetación

-

El software se ha testeado en una Raspberry Pi con:
* Gateway para la recepción de datos en red DigiMesh
* Módulo 3G para el envío de paquete de datos entre fechas a un FTP.

 






