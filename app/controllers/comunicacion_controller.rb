class ComunicacionController < ApplicationController

  require 'net/http'
  require 'uri'
  require 'csv'

  ##crontab
  ## java -classpath .:./lib/*:./bin es.iecolab.wsn.ReadData /dev/tty.usbserial-A6015JWE 38400 http://localhost:3000/api/new_data_mote 2>&1 >> output.log

  def index

    render :controller => :comunicacion, :action => :show_log

  end


  def log
    path = Rails.root.to_s+"/xbee/output.log"
    render(:text => "<pre>"+CGI::escapeHTML(`tail #{path} -n 10`)+"</pre>")
  end

  def show_log
  end

  def confirmar_paquete_datos
    desde = (Date.parse(params[:desde]) if params[:desde]) || nil
    hasta = (Date.parse(params[:hasta]) if params[:hasta]) || Date.today

    t = Transmision.create(:desde_fecha => desde, :hasta_fecha => hasta, :fecha => Date.today, :origen => "SI")

    render :text => "Operación concluida con éxito" and return true

  end

  def iniciar_sincronizacion

    mi_url = url_for(:controller => "/comunicacion", :action => "paquete_datos")

    conf = Configuracion.last

    url = conf.url_sistema_informacion+"/clima/cli_wsns/iniciar_sincronizacion?api_key=#{conf.api_key}&servidor=#{mi_url}&codigo=#{conf.codigo_wsn}"
    uri = URI.parse(url)
    respuesta = Net::HTTP.get(uri)

    render :text => respuesta and return true

  end

  def paquete_datos
    desde = params[:desde] || nil
    hasta = params[:hasta] || nil

    ## obtenemos las fechas deseadas

    if desde == nil
      ult_transmision = Transmision.last
      if ult_transmision == nil
        desde = Dato.first.fecha.to_date
      else
        desde = ult_transmision.hasta_fecha + 1
      end
    else
      desde = Date.parse(desde)
    end

    if hasta == nil
      hasta = Date.today
    else
      hasta = Date.parse(hasta)
    end

    ## generamos la carpeta temporal donde vamos a guardar las cosas
    nombre_dir = (0...8).map{ (65 + rand(26)).chr }.join
    path = File.join(Rails.root.to_s, "tmp", nombre_dir)
    Dir.mkdir(path)

    path_imagenes = File.join(path, "imagenes")
    Dir.mkdir(path_imagenes)

    ## generamos el csv con los datos
    datos = Dato.where("fecha >= ? and fecha <= ?", desde, hasta)

    @csv = CSV.generate do |csv|
      csv << ["id", "fecha", "variable_id", "valor", "dispositivo_id"] # cabecera
      datos.each { |dato|
        csv << [dato.id, dato.fecha, dato.variable.to_s, dato.valor, dato.dispositivo.to_s]
      }
    end

    File.open(File.join(path, "datos.csv"), "w+") do |f|
      f.write(@csv)
    end

    ## empaquetamos las imágenes y su csv
    imagenes = Imagen.where("fecha >= ? and fecha <= ?", desde, hasta)
    @csv = CSV.generate(:col_sep => ";#;", :row_sep => "###") do |csv|
      csv << ["id", "fecha", "descripcion", "imagen_1", "imagen_2", "imagen_p", "lat", "lng"] # cabecera
      imagenes.each { |dato|
        imagen_1 = "imagen_1"+File.extname(dato.imagen_1.path)
        imagen_2 = "imagen_2"+File.extname(dato.imagen_2.path)
        imagen_p = "imagen_p"+File.extname(dato.imagen_p.path)
        temp_imagenes = File.join(path_imagenes, dato.id.to_s)
        Dir.mkdir(temp_imagenes)
        FileUtils.copy(dato.imagen_1.path, File.join(temp_imagenes, imagen_1))
        FileUtils.copy(dato.imagen_2.path, File.join(temp_imagenes, imagen_2))
        FileUtils.copy(dato.imagen_p.path, File.join(temp_imagenes, imagen_p))
        csv << [dato.id, dato.fecha, dato.descripcion, imagen_1, imagen_2, imagen_p, dato.lat, dato.lng]
      }
    end

    File.open(File.join(path, "imagenes.csv"), "w+") do |f|
      f.write(@csv)
    end

    ## comprimimos el directorio
    ##Dir.chdir(path)
    system("cd #{path} && zip -D -r #{nombre_dir}.zip *")

    ## borramos el temporal
    #system("rm -r #{path}")

    ## enviamos el comprimido
    send_file File.join(path, "#{nombre_dir}.zip"), :filename => "paquete.zip"

  end

end
