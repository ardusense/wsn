# -*- coding: utf-8 -*-
class ConfiguracionsController < ApplicationController

  active_scaffold :"configuracion" do |conf|
    conf.label = "Configuración"

    conf.create.columns << :xbee_pan_id
    conf.update.columns << :xbee_pan_id

    conf.actions.exclude :create, :delete

    conf.list.columns = [:url_sistema_informacion, :xbee_port, :lat, :lng]

  end


end
