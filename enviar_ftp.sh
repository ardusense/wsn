#!/bin/bash

SERVER=linaria.iecolab.es
USERNAME=usuario
PASSWORD=password
PATH_FTP='/web/wsn/canar'
PATH_LOCAL='ftp'
FECHA=`date +"%Y_%j"`
MENSAJE="Datos enviados por FTP para la fecha $FECHA. Raspberry iEcolab"
NUMERO=num_telefono

echo 'Comprobamos el puerto de comunicacion del modem ... '
wvdialconf /etc/wvdial.conf

echo 'Obtenemos el puerto en el que esta conectado el modem ... '
PUERTO=`cat /etc/wvdial.conf | grep "Modem =" | awk '/=/ {print $3}'`
_PUERTO="${PUERTO//\//\\/}"

echo 'Modificamos el fichero de configuracion para el envio de SMS ... '
cat $PATH_LOCAL/gammurc.conf.new | sed -e "s/PUERTO/${_PUERTO}/g" > $PATH_LOCAL/gammurc.conf

echo 'Conectamos a Internet ... '
eval "wvdial &"
PID_3G=$!

echo 'Generando el fichero a enviar ... '
curl http://localhost/comunicacion/paquete_datos > $PATH_LOCAL/$FECHA.zip

echo 'Esperamos un tiempo prudencial para que tengamos cobertura ... '
sleep 10s

echo "Enviando por FTP al servidor"
ncftpput -DD -m -u $USERNAME -p $PASSWORD $SERVER $PATH_FTP $PATH_LOCAL/*.zip

echo 'Desconectamos el 3G ... '
poff
#eval "kill -1 $PID_3G"

echo 'Validamos los datos enviados en el sistema ... '
curl http://localhost/comunicacion/confirmar_paquete_datos > $PATH_LOCAL/confirmacion_datos.log

#echo "Esperamos a que las comunicaciones 3G terminen ... "
#while ps -p `cat $PID_FILE` > /dev/null; do sleep 1; done

echo 'Enviamos el SMS ... '
echo $MENSAJE | gammu -c $PATH_LOCAL/gammurc.conf --sendsms text $NUMERO



